MarkSport!
==========

## Easy HTML Export for Documentation

I love Markdown and I love using it for fast and easy documentation. Unfortunately, there wasn't a great way to create one single HTML file that had the rendered markdown, stylesheets, and javascript embedded. Until now.

MarkSport is a stupid application with one purpose: To give you a single HTML file with all the stylish trimmings, that you can archive or send to people. Think of the single file like a lightweight PDF.

Why not just use PDF? Some people don't like them, some people like the nice way bootstrap renders on mobile devices, some people just prefer HTML.
