require 'sinatra'
require 'redcarpet'
require 'erb'
require 'haml'

set :haml, :format => :html5
set :erb, :layout_engine => :haml
set :markdown, :layout_engine => :haml

get '/' do
  haml :index
end

get '/convert' do
  haml :convert
end

post '/convert' do
  @markdown = params[:markdown]
  haml :export, :layout_engine => :erb
end

get '/formatting' do
  erb :formatting
end

get '/about' do
  haml :about
end
